CREATE TABLE [CultureIndex].[Result]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[SurveyId] [int] NOT NULL,
[Trait_A] [int] NULL,
[Trait_B] [int] NULL,
[Trait_C] [int] NULL,
[Trait_D] [int] NULL,
[Trait_L] [int] NULL,
[Trait_I] [int] NULL,
[Trait_EU] [int] NULL,
[Trait_Line] [float] NULL,
[AssessmentDate] [date] NOT NULL,
[JobTitle] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TraitPattern] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobPattern] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonId] [int] NOT NULL,
[Behavior_A] [int] NULL,
[Behavior_B] [int] NULL,
[Behavior_C] [int] NULL,
[Behavior_D] [int] NULL,
[Behavior_L] [int] NULL,
[Behavior_I] [int] NULL,
[Behavior_EU] [int] NULL,
[Behavior_Line] [int] NULL,
[Survey_Hash] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
