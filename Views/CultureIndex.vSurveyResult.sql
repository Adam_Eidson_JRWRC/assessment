SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Script for SelectTopNRows command from SSMS  ******/
CREATE View [CultureIndex].[vSurveyResult] as
SELECT email,
name,
SurveyId
,[Trait_A]
,[Trait_B]
,[Trait_C]
,[Trait_D]
,[Trait_L]
,[Trait_I]
,[Trait_EU]
,[Trait_Line]
,[AssessmentDate]
,[JobTitle]
,[TraitPattern]
,[JobPattern]
,[Behavior_A]
,[Behavior_B]
,[Behavior_C]
,[Behavior_D]
,[Behavior_L]
,[Behavior_I]
,[Behavior_EU]
,[Behavior_Line]
,Survey_Hash
  FROM [Assessment].[dbo].[Person] p
  join Assessment.CultureIndex.Result r on p.id = r.PersonId
GO
