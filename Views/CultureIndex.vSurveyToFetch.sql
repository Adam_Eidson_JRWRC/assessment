SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [CultureIndex].[vSurveyToFetch] as
select distinct surveyId
from Assessment.CultureIndex.Result
where Trait_A is null and Behavior_A is null
GO
